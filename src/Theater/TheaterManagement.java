package Theater;


public class TheaterManagement {
	
	Seat seat;
	double [][] arrSeatAvailable;
	
	public TheaterManagement()
	{
		seat = new Seat();
		arrSeatAvailable = seat.getSeatAvailable();
	}
	
	public Seat getSeat()
	{
		return seat;
	}
	
	public void buySeat(int row, int col) 
	{
		arrSeatAvailable[row][col] = 0;
	}
	
	public int getRow()
	{
		return DataSeatPrice.ROW;		
	}
	
	public int getCol()
	{
		return DataSeatPrice.COL;
	}
	
	public double[][] getSeatAvailable()
	{
		return arrSeatAvailable;
	}
}
