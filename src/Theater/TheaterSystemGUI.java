package Theater;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class TheaterSystemGUI implements ActionListener 
{
	private JButton[][] arrButton;
	
	private TheaterManagement tm;
	
	private JFrame fme;
	private JPanel pnlSeat;	
	private JPanel pnlChoose;
	private JPanel pnlPrice;
	
	
	private JTextField txtChooseSeatPrice;
	private JButton btnChooseSeatPrice;
	private JLabel lblChooseSeatPrice;
	
	private JLabel lblLabelPrice;
	private JLabel lblSeat;
	private JLabel lblPrice;
	private JButton btnBuy;
	private JButton btnEmptySeat;
	
	private JButton btnTemp;
	private int intRowSelected,intColSelected;
		
	public void showFrame()
	{
		tm = new TheaterManagement();
		arrButton = new JButton[tm.getRow()][tm.getCol()];
		btnTemp = new JButton();
		
		intRowSelected = 0;
		intColSelected = 0;
		
		fme = new JFrame("Lab 7");
		pnlChoose = new JPanel();
		pnlPrice = new JPanel();
				
		lblChooseSeatPrice = new JLabel("   Input Seat Price");
		txtChooseSeatPrice = new JTextField("",10);
		btnChooseSeatPrice = new JButton("OK");	
		
		lblLabelPrice = new JLabel("   Price");
		lblSeat = new JLabel("   Seat: ");
		lblPrice = new JLabel("0.00 Baht");
		btnBuy = new JButton("Buy");		
		btnEmptySeat = new JButton("SeatAvailable");
		
		pnlChoose.setLayout(new GridLayout(1,3));
		pnlChoose.setMaximumSize(new Dimension(180,50));
		btnChooseSeatPrice.addActionListener(this);		
		pnlChoose.add(lblChooseSeatPrice);
		pnlChoose.add(txtChooseSeatPrice);
		pnlChoose.add(btnChooseSeatPrice);
		
		pnlPrice.setLayout(new GridLayout(2, 3));
		pnlPrice.setMaximumSize(new Dimension(50,50));
		btnBuy.addActionListener(this);
		
		btnEmptySeat.addActionListener(this);
		pnlPrice.add(lblSeat);
		pnlPrice.add(lblLabelPrice);		
		pnlPrice.add(lblPrice);
		pnlPrice.add(btnBuy);		
		pnlPrice.add(btnEmptySeat);
		pnlPrice.setVisible(true);
		
		fme.setSize(1200, 700);
		fme.setLayout(new BorderLayout());
		fme.add(setPanel(tm.getSeat().getSeatAvailable(),"PRICE") , BorderLayout.NORTH);
		JPanel pnlTmp = new JPanel();
		pnlTmp.setLayout(new BorderLayout());
		pnlTmp.add(pnlChoose,BorderLayout.NORTH);
		pnlTmp.add(pnlPrice,BorderLayout.WEST);
		
		fme.add(pnlTmp,BorderLayout.WEST);
				
		fme.setVisible(true);
	}
	
	public JPanel setPanel(double arrSeat[][],String strType)
	{
		int i,j;
		JPanel pnlTheater = new JPanel();
		pnlTheater.setLayout(new GridLayout(16,21));
		
		pnlTheater.setSize(600,600);
		
		for(i=0;i<=tm.getRow();i++)
		{
			for(j=0;j<=tm.getCol();j++)
			{				
				if(i==0) // Insert Col's label
				{
					if(j==0)
						pnlTheater.add(new JLabel("  "),i,j);
					else
						pnlTheater.add(new JLabel(""+j),i,j);
				}
				else if(j==0) // Insert Label Row's Label
				{
					pnlTheater.add(new JLabel(""+ (char)(65+i-1)));
				}
				else
				{
					int tmpPrice = (int)arrSeat[i-1][j-1];
					JButton btnTmp;
					if(tmpPrice>0)
					{
						if(strType.equals("PRICE"))
						{
							btnTmp = new JButton(tmpPrice+"");
							arrButton[i-1][j-1] = btnTmp;
							btnTmp.addActionListener(this);
							pnlTheater.add(btnTmp);	
						}
						else if(strType.equals("NAME"))
						{
							btnTmp = new JButton(tm.getSeat().getSeatName(i-1, j-1)+"");
							arrButton[i-1][j-1] = btnTmp;
							btnTmp.addActionListener(this);
							pnlTheater.add(btnTmp);	
						}
						
					}
					else if(j==9 || j==10 ||tmpPrice <= 0 ) //tmpPrice <= 0
					{
						btnTmp = new JButton("");
						btnTmp.setEnabled(false);
						pnlTheater.add(btnTmp);	
					}							
				}
			}
		}		
		pnlTheater.setVisible(true);
		return  pnlTheater;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		String arg = e.getActionCommand();
		Object source = e.getSource();
				
		if(source instanceof JButton)
		{
			JButton btn = (JButton)source;
			
			if("OK".equals(arg))
			{
				getSeatAvailable();
				if(txtChooseSeatPrice.getText()!="")
				{
					double price = Double.parseDouble(txtChooseSeatPrice.getText());
					SetAvailablePrice(price);
				}				
			}
			else if("Buy".equals(arg))
			{	
				btnTemp.setEnabled(false);
				lblPrice.setText("0.00 Baht");
				lblSeat.setText("   Seat");
				tm.buySeat(intRowSelected, intColSelected);
			}
			else if("SeatAvailable".equals(arg))
			{
				getSeatAvailable();				
			}
			else
			{	
				btnTemp = btn;
				lblPrice.setText(btn.getText()+".00 Baht");
				lblSeat.setText("   Seat:  "+findButtonPosition());		
			}
		}
	}
	public String findButtonPosition()
	{
		String strSeatPosition="";
		int i,j;
		for(i=0; i<tm.getRow(); i++)
		{
			for(j=0; j<tm.getCol(); j++)
			{
				if(btnTemp == arrButton[i][j])
				{
					strSeatPosition = tm.getSeat().getSeatName(i+1, j+1);
					intRowSelected = i;
					intColSelected = j;
				}
			}
		}
		return strSeatPosition;
	}
	
	public void SetAvailablePrice(double price)
	{
		int i,j;
		double[][] arrSeatAvailable = tm.getSeatAvailable().clone();
		
		for(i=0; i<tm.getRow(); i++)
		{
			for(j=0; j<tm.getCol(); j++)
			{
				if(arrSeatAvailable[i][j]!= price )
				{					
					JButton btnTmp = arrButton[i][j];
					if (btnTmp!= null)
					{
						btnTmp.setEnabled(false);
					}					
				}
			}
		}
	}
	
	public void getSeatAvailable()
	{
		int i,j;				
		double[][] arrSeatAvailable = tm.getSeatAvailable().clone();
		
		for(i=0; i<tm.getRow(); i++)
		{
			for(j=0; j<tm.getCol(); j++)
			{
				JButton btnTmp = arrButton[i][j];
				if(btnTmp!= null)
				{
					if(arrSeatAvailable[i][j]>0)
					{								
						btnTmp.setEnabled(true);
					}
					else 
					{
						btnTmp.setEnabled(false);
					}
				}
			}
		}
	}
}
