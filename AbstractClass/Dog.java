package AbstractClass;

public class Dog extends Animal {

	public Dog(String name) {
		super(name);		
	}

	@Override
	public void roar() {
		System.out.println(this.getName() + " Bow Bow");
		
	}
	
	public void run()
	{
		super.run();
		System.out.println("Dog Running");
	}
	
	public void run(int speed)
	{
		System.out.println("Dog fast Running");
	}


}
