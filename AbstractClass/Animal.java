package AbstractClass;

public abstract class Animal {
	private String name;
	
	public Animal(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void run()
	{
		System.out.println("Animal Running");
	}
	
	public abstract void roar();

}
